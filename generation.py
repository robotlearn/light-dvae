"""
Software TO BE DONE
Copyright Inria
Year 2023
Contact : xiaoyu.lin@inria.fr
License agreement in LICENSE.txt
"""

import os
os.environ[ 'NUMBA_CACHE_DIR' ] = '/tmp/'
import math
import torch
import copy
import json
import librosa
import numpy as np
import pandas as pd
import soundfile as sf
from torch.utils.tensorboard import SummaryWriter
from src.model import light_dvae, hit_dvae
from src.dataset import speech_dataset
from src.utils import option, optim_goal, utils
import librosa.display
import matplotlib.pyplot as plt


args = option.get_args_parser()
torch.manual_seed(args.seed)
np.random.seed(args.seed)

## resume
if args.resume_pth:
    ckpt = torch.load(args.resume_pth, map_location='cuda')
    resume_pth = args.resume_pth
    args.out_dir = os.path.join(args.out_dir, f'{args.exp_name}')
    os.makedirs(args.out_dir, exist_ok=True)
    
else:
    resume_pth = None
    args.out_dir = os.path.join(args.out_dir, f'{args.exp_name}')
    os.makedirs(args.out_dir, exist_ok=True)


## logger
logger = utils.get_logger(args.out_dir)
if resume_pth:
    logger.info('loading checkpoint from {}'.format(resume_pth))
else:
    logger.info(json.dumps(vars(args), indent=4, sort_keys=True))

## network
if args.model_name == 'light_dvae':
    net = light_dvae.LigHTDVAE(args)
elif args.model_name == 'hit_dvae':
    net = hit_dvae.HitDVAE(args)
net.eval()
net.cuda()
logger.info('Loading network...')
logger.info('Total params: %.2fM' % (sum(p.numel() for p in net.parameters()) / 1000000.0))

## checkpoint
if resume_pth:
    net.load_state_dict(ckpt['net_state'])

fs = int(16e3)
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
zp_percent = 0
wlen = wlen_sec*fs # window length of 64 ms
wlen = int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
hop = int(hop_percent*wlen) # hop size
nfft = wlen + zp_percent*wlen # number of points of the DFT
win = np.sin(np.arange(.5,wlen-.5+1)/wlen*np.pi); # sine analysis window

num_batches = int(args.num_generate // args.batch_size)
stft_gen_path = os.path.join(args.out_dir, 'stft_gen')
os.makedirs(stft_gen_path, exist_ok=True)

num_gen = 1
for idx in range(num_batches):
    with torch.no_grad():
        generated_sequences = net.generation_from_prior()
    seq_len, bs, x_dim = generated_sequences.shape
    for i in range(bs):

        plt.figure()
        librosa.display.specshow(librosa.power_to_db(generated_sequences[:, i, :].detach().cpu().numpy().squeeze().T), 
            y_axis='linear', sr=fs, hop_length=hop, vmin=-40, vmax=40)
        plt.set_cmap('magma')
        plt.colorbar()
        plt.title('generated spectrogram')
        plt.savefig(os.path.join(stft_gen_path, 'generated_speech_{}.png'.format(num_gen)))

        generated_power = generated_sequences[:, i, :].detach().permute(1,0)
        generated_mag = torch.sqrt(generated_power).cpu().numpy()

        temporal = librosa.griffinlim(generated_mag, hop_length=hop, 
                                    win_length=wlen, window=win, n_fft=nfft, center=True, 
                                    pad_mode='reflect')

        sf.write(os.path.join(args.out_dir, 'generated_speech_%05d.wav' % (num_gen)), temporal, fs)

        spec_after_griffinlim = librosa.stft(temporal, n_fft=nfft, hop_length=hop, win_length=wlen, window=win)
        X_abs_2 = np.abs(spec_after_griffinlim)**2

        num_gen+=1

logger.info('Generation finished.')
logger.info('Generated %d sequences.' % (num_gen))


