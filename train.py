"""
Software TO BE DONE
Copyright Inria
Year 2023
Contact : xiaoyu.lin@inria.fr
License agreement in LICENSE.txt
"""

import os
os.environ[ 'NUMBA_CACHE_DIR' ] = '/tmp/'
import math
import torch
import copy
import json
import librosa
import numpy as np
import pandas as pd
import soundfile as sf
from torch.utils.tensorboard import SummaryWriter
from src.model import hit_dvae, light_dvae
from src.dataset import speech_dataset
from src.utils import option, optim_goal, utils
import matplotlib.pyplot as plt
import librosa.display


def evaluation(args, net, dataset, val_writer, current_iter, best_eval, best_iter):
    isd_ar = []
    isd_tf = []
    sisdr_ar = []
    sisdr_tf = []

    eval_sisdr = optim_goal.EvalMetrics(metric='sisdr')
    net.eval()

    # STFT
    file_list = librosa.util.find_files(args.test_dir)
    wlen = args.wlen_sec * args.fs
    wlen = int(np.power(2, np.ceil(np.log2(wlen)))) # power of 2
    hop = int(args.hop_percent * wlen)
    nfft = wlen + args.zp_percent * wlen
    win = torch.sin(torch.arange(0.5, wlen+0.5) / wlen * np.pi)
    fs = args.fs

    data_loader = dataset.batch_generator(seq_len=args.seq_len, hop=hop, batch_size=args.batch_size)
    for batch_data in data_loader:
        batch_X = torch.stft(torch.from_numpy(batch_data), n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True, 
                    pad_mode='reflect', normalized=False, onesided=True, return_complex=True)

        data_orig = (batch_X.abs().float() ** 2).to(args.device)
        data_orig = data_orig.permute(2, 0, 1) # (bs, x_dim, T) -> (T, bs, x_dim)
        with torch.no_grad():
            z, z_mean, z_logvar, w, w_mean, w_logvar = net.inference(data_orig)
            data_recon_ar = net.generation_x(z, w)
            data_recon_tf, _, _ = net.generation(data_orig, z, w)
            data_recon_tf = torch.exp(data_recon_tf)

            loss_isd_ar = optim_goal.loss_isd(data_orig, data_recon_ar)
            loss_isd_tf = optim_goal.loss_isd(data_orig, data_recon_tf)
            isd_ar.append(loss_isd_ar.item())
            isd_tf.append(loss_isd_tf.item())

        batch_X_ang = batch_X.angle()
        data_recon_ar = data_recon_ar.to('cpu').detach().permute(1, 2, 0)
        data_recon_tf = data_recon_tf.to('cpu').detach().permute(1, 2, 0)
        data_recon_ar_mod = torch.sqrt(data_recon_ar)
        data_recon_tf_mod = torch.sqrt(data_recon_tf)
        batch_X_ar = data_recon_ar_mod * torch.exp(1j * batch_X_ang)
        batch_X_tf = data_recon_tf_mod * torch.exp(1j * batch_X_ang)

        batch_x_ar = torch.istft(batch_X_ar, n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True,
                    normalized=False, onesided=True, return_complex=False).numpy()
        batch_x_tf = torch.istft(batch_X_tf, n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True,
                    normalized=False, onesided=True, return_complex=False).numpy()

        for i in range(args.batch_size):
            sisdr_ar.append(eval_sisdr.eval(batch_x_ar[i], batch_data[i], fs))
            sisdr_tf.append(eval_sisdr.eval(batch_x_tf[i], batch_data[i], fs))

    avg_isd_ar = np.mean(isd_ar)
    avg_isd_tf = np.mean(isd_tf)
    avg_sisdr_ar = np.mean(sisdr_ar)
    avg_sisdr_tf = np.mean(sisdr_tf)
    val_writer.add_scalar('Val/loss_isd_ar', avg_isd_ar, global_step=current_iter)
    val_writer.add_scalar('Val/loss_isd_tf', avg_isd_tf, global_step=current_iter)
    val_writer.add_scalar('Val/loss_sisdr_ar', avg_sisdr_ar, global_step=current_iter)
    val_writer.add_scalar('Val/loss_sisdr_tf', avg_sisdr_tf, global_step=current_iter)

    if avg_sisdr_ar > best_eval:
        best_eval = avg_sisdr_ar
        best_iter = current_iter
        save_best = True
    else:
        save_best = False

    net.train()

    msg = "Evaluation {:d} : SI-SDR ar. {:.5f} (best. {:.5f} @ iter{:d}) ".format(current_iter, avg_sisdr_ar, best_eval, best_iter)

    return msg, save_best, best_iter, best_eval



def compute_acc_err(x_recon, x):
    x_dim = x.shape[1]
    std = x.std(dim=1)
    mean = x.mean(dim=1)
    eps = np.finfo(float).eps

    l1_std_norm = torch.sum((x_recon - x).abs(), dim=1) / (x_dim * std)
    l2_std_norm = torch.sqrt(torch.sum((x_recon - x)**2, dim=1) / (x_dim * (std**2)))
    l1_mean_norm = torch.sum((x_recon - x).abs(), dim=1) / (x_dim * mean)
    l2_mean_norm = torch.sqrt(torch.sum((x_recon - x)**2, dim=1) / (x_dim * (mean**2)))
    l1_x_norm = torch.sum((x_recon - x).abs()/(x+eps), dim=1) / x_dim
    l1_x_recon_norm = torch.sum((x_recon - x).abs()/(x_recon+eps), dim=1) / x_dim
    l2_x_norm = torch.sqrt(torch.sum(((x_recon - x)/(x+eps))**2, dim=1) / x_dim)
    l2_x_recon_norm = torch.sqrt(torch.sum(((x_recon - x)/(x_recon+eps))**2, dim=1) / x_dim)

    x = x + 1e-10
    isd = torch.sum( x/x_recon - torch.log(x/x_recon) - 1, dim=1) / x_dim

    return l1_std_norm, l2_std_norm, l1_mean_norm, l2_mean_norm, l1_x_norm, l2_x_norm, l1_x_recon_norm, l2_x_recon_norm, isd

def test(args, net, logger, test_val=False):
    list_rmse_ar = []
    list_sisdr_ar = []
    list_pesq_ar = []
    list_pesq_wb_ar = []
    list_pesq_nb_ar = []
    list_estoi_ar = []

    list_rmse_tf = []
    list_sisdr_tf = []
    list_pesq_tf = []
    list_pesq_wb_tf = []
    list_pesq_nb_tf = []
    list_estoi_tf = []
    eval_metrics = optim_goal.EvalMetrics(metric='all')
    net.eval()

    if test_val:
        logger.info('Evaluation on validation dataset...')
        file_list = librosa.util.find_files(args.val_dir)
        logger.info('Total samples {}'.format(len(file_list)))
    else:
        logger.info('Evaluation on test dataset...')
        file_list = librosa.util.find_files(args.test_dir)
        logger.info('Total samples {}'.format(len(file_list)))
    
    # STFT    
    wlen = args.wlen_sec * args.fs
    wlen = int(np.power(2, np.ceil(np.log2(wlen)))) # pwoer of 2
    hop = int(args.hop_percent * wlen)
    nfft = wlen + args.zp_percent * wlen
    win = torch.sin(torch.arange(0.5, wlen+0.5) / wlen * np.pi)
    fs = args.fs

    logger.info('='*80)
    logger.info('fs: {}'.format(fs))
    logger.info('wlen: {}'.format(wlen))
    logger.info('hop: {}'.format(hop))
    logger.info('nfft: {}'.format(nfft))
    logger.info('='*80)

    for audio_file in file_list:

        x, fs_x = sf.read(audio_file)
        x, _ = librosa.effects.trim(x, top_db=30)
        x = x / np.max(np.abs(x))
        
        # STFT
        X = torch.stft(torch.from_numpy(x), n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True, 
                    pad_mode='reflect', normalized=False, onesided=True, return_complex=True)
        
        X_mag = X.abs().float() ** 2

        # Forward
        data_orig = X_mag.to(args.device)
        data_orig = data_orig.permute(1,0).unsqueeze(1) #  (x_dim, T) => (T, 1, x_dim)
        with torch.no_grad():
            z, z_mean, z_logvar, w, w_mean, w_logvar = net.inference(data_orig)
            data_recon_ar = net.generation_x(z, w)
            data_recon_tf, _, _ = net.generation(data_orig, z, w)
            data_recon_tf = data_recon_tf.exp()

        data_recon_ar = data_recon_ar.to('cpu').detach().squeeze().permute(1,0)
        data_recon_tf = data_recon_tf.to('cpu').detach().squeeze().permute(1,0)

        # iSTFT for ar
        X_ang = X.angle()
        X_mag_recon_ar = torch.sqrt(data_recon_ar)
        X_recon_ar = X_mag_recon_ar * torch.exp(1j * X_ang)
        x_recon_ar = torch.istft(X_recon_ar, n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True,
                    normalized=False, onesided=True, return_complex=False).numpy()

        rmse_ar, sisdr_ar, pesq_ar, pesq_wb_ar, pesq_nb_ar, estoi_ar = eval_metrics.eval(x_est=x_recon_ar, x_ref=x, fs=fs_x)

        list_rmse_ar.append(rmse_ar)
        list_sisdr_ar.append(sisdr_ar)
        list_pesq_ar.append(pesq_ar)
        list_pesq_wb_ar.append(pesq_wb_ar)
        list_pesq_nb_ar.append(pesq_nb_ar)
        list_estoi_ar.append(estoi_ar)

        # iSTFT for tf
        X_mag_recon_tf = torch.sqrt(data_recon_tf)
        X_recon_tf = X_mag_recon_tf * torch.exp(1j * X_ang)
        x_recon_tf = torch.istft(X_recon_tf, n_fft=nfft, hop_length=hop, 
                    win_length=wlen, window=win, center=True,
                    normalized=False, onesided=True, return_complex=False).numpy()

        rmse_tf, sisdr_tf, pesq_tf, pesq_wb_tf, pesq_nb_tf, estoi_tf = eval_metrics.eval(x_est=x_recon_tf, x_ref=x, fs=fs_x)

        list_rmse_tf.append(rmse_tf)
        list_sisdr_tf.append(sisdr_tf)
        list_pesq_tf.append(pesq_tf)
        list_pesq_wb_tf.append(pesq_wb_tf)
        list_pesq_nb_tf.append(pesq_nb_tf)
        list_estoi_tf.append(estoi_tf)

    np_rmse_ar = np.array(list_rmse_ar)
    np_sisdr_ar = np.array(list_sisdr_ar)
    np_pesq_ar = np.array(list_pesq_ar)
    np_pesq_wb_ar = np.array(list_pesq_wb_ar)
    np_pesq_nb_ar = np.array(list_pesq_nb_ar)
    np_estoi_ar = np.array(list_estoi_ar)

    logger.info('Re-synthesis finished')

    logger.info("Mean evaluation")
    logger.info('mean rmse score: {:.4f}'.format(np.mean(np_rmse_ar)))
    logger.info('mean sisdr score: {:.1f}'.format(np.mean(np_sisdr_ar)))
    logger.info('mean pypesq wb score: {:.2f}'.format(np.mean(np_pesq_ar)))
    logger.info('mean pesq wb score: {:.2f}'.format(np.mean(np_pesq_wb_ar)))
    logger.info('mean pesq nb score: {:.2f}'.format(np.mean(np_pesq_nb_ar)))
    logger.info('mean estoi score: {:.2f}'.format(np.mean(np_estoi_ar)))

    rmse_median_ar, rmse_ci_ar = optim_goal.compute_median(np_rmse_ar)
    sisdr_median_ar, sisdr_ci_ar = optim_goal.compute_median(np_sisdr_ar)
    pesq_median_ar, pesq_ci_ar = optim_goal.compute_median(np_pesq_ar)
    pesq_wb_median_ar, pesq_wb_ci_ar = optim_goal.compute_median(np_pesq_wb_ar)
    pesq_nb_median_ar, pesq_nb_ci_ar = optim_goal.compute_median(np_pesq_nb_ar)
    estoi_median_ar, estoi_ci_ar = optim_goal.compute_median(np_estoi_ar)

    logger.info("Median evaluation")
    logger.info('median rmse score: {:.4f} +/- {:.4f}'.format(rmse_median_ar, rmse_ci_ar))
    logger.info('median sisdr score: {:.1f} +/- {:.1f}'.format(sisdr_median_ar, sisdr_ci_ar))
    logger.info('median pypesq score: {:.2f} +/- {:.2f}'.format(pesq_median_ar, pesq_ci_ar))
    logger.info('median pesq wb score: {:.2f} +/- {:.2f}'.format(pesq_wb_median_ar, pesq_wb_ci_ar))
    logger.info('median pesq nb score: {:.2f} +/- {:.2f}'.format(pesq_nb_median_ar, pesq_nb_ci_ar))
    logger.info('median estoi score: {:.2f} +/- {:.2f}'.format(estoi_median_ar, estoi_ci_ar))

    np_rmse_tf = np.array(list_rmse_tf)
    np_sisdr_tf = np.array(list_sisdr_tf)
    np_pesq_tf = np.array(list_pesq_tf)
    np_pesq_wb_tf = np.array(list_pesq_wb_tf)
    np_pesq_nb_tf = np.array(list_pesq_nb_tf)
    np_estoi_tf = np.array(list_estoi_tf)

    logger.info("Mean evaluation using tf")
    logger.info('mean rmse_tf score: {:.4f}'.format(np.mean(np_rmse_tf)))
    logger.info('mean sisdr_tf score: {:.1f}'.format(np.mean(np_sisdr_tf)))
    logger.info('mean pypesq_tf wb score: {:.2f}'.format(np.mean(np_pesq_tf)))
    logger.info('mean pesq_tf wb score: {:.2f}'.format(np.mean(np_pesq_wb_tf)))
    logger.info('mean pesq_tf nb score: {:.2f}'.format(np.mean(np_pesq_nb_tf)))
    logger.info('mean estoi_tf score: {:.2f}'.format(np.mean(np_estoi_tf)))

    rmse_median_tf, rmse_ci_tf = optim_goal.compute_median(np_rmse_tf)
    sisdr_median_tf, sisdr_ci_tf = optim_goal.compute_median(np_sisdr_tf)
    pesq_median_tf, pesq_ci_tf = optim_goal.compute_median(np_pesq_tf)
    pesq_wb_median_tf, pesq_wb_ci_tf = optim_goal.compute_median(np_pesq_wb_tf)
    pesq_nb_median_tf, pesq_nb_ci_tf = optim_goal.compute_median(np_pesq_nb_tf)
    estoi_median_tf, estoi_ci_tf = optim_goal.compute_median(np_estoi_tf)

    logger.info("Median evaluation using tf")
    logger.info('median rmse_tf score: {:.4f} +/- {:.4f}'.format(rmse_median_tf, rmse_ci_tf))
    logger.info('median sisdr_tf score: {:.1f} +/- {:.1f}'.format(sisdr_median_tf, sisdr_ci_tf))
    logger.info('median pypesq_tf score: {:.2f} +/- {:.2f}'.format(pesq_median_tf, pesq_ci_tf))
    logger.info('median pesq_tf wb score: {:.2f} +/- {:.2f}'.format(pesq_wb_median_tf, pesq_wb_ci_tf))
    logger.info('median pesq_tf nb score: {:.2f} +/- {:.2f}'.format(pesq_nb_median_tf, pesq_nb_ci_tf))
    logger.info('median estoi_tf score: {:.2f} +/- {:.2f}'.format(estoi_median_tf, estoi_ci_tf))



args = option.get_args_parser()
torch.manual_seed(args.seed)
np.random.seed(args.seed)

## resume
if args.resume_pth:
    ckpt = torch.load(args.resume_pth, map_location='cuda')
    resume_pth = args.resume_pth
    args.out_dir = os.path.join(args.out_dir, f'{args.exp_name}')
    os.makedirs(args.out_dir, exist_ok=True)
    
else:
    resume_pth = None
    args.out_dir = os.path.join(args.out_dir, f'{args.exp_name}')
    os.makedirs(args.out_dir, exist_ok=True)

## logger
logger = utils.get_logger(args.out_dir)
if resume_pth:
    logger.info('loading checkpoint from {}'.format(resume_pth))
else:
    logger.info(json.dumps(vars(args), indent=4, sort_keys=True))

## tensorboard
tb_dir_tr = os.path.join(args.out_dir, 'log/training')
os.makedirs(tb_dir_tr, exist_ok=True)
tb_dir_val = os.path.join(args.out_dir, 'log/val')
os.makedirs(tb_dir_val, exist_ok=True)
tr_writer = SummaryWriter(tb_dir_tr)
val_writer = SummaryWriter(tb_dir_val)

# dataloader
trainloader, train_num = speech_dataset.TrainLoader(args)
valdataset, val_num = speech_dataset.ValDataset(args)
logger.info('Loading dataset: {}'.format(args.dataset_name))
logger.info('Train: {:d} audio sequences'.format(train_num))
logger.info('Val: {:d} audio sequences'.format(val_num))

## network
if args.model_name == 'light_dvae':
    net = light_dvae.LigHTDVAE(args)
elif args.model_name == 'hit_dvae':
    net = hit_dvae.HitDVAE(args)
net.train()
net.cuda()
logger.info('Loading network...')
logger.info('Total params: %.2fM' % (sum(p.numel() for p in net.parameters()) / 1000000.0))

## optimizer
if args.decay_option == 'all':
    optimizer = torch.optim.AdamW(net.parameters(), lr=args.min_lr, weight_decay=args.weight_decay)
elif args.decay_option == 'light':
    optimizer = utils.Configure_AdamW(net, args.weight_decay, args.min_lr)

scheduler = utils.get_scheduler(optimizer, args)

## checkpoint
if resume_pth:
    net.load_state_dict(ckpt['net_state'])
    optimizer.load_state_dict(ckpt['optim_state'])
    scheduler.load_state_dict(ckpt['sched_state'])
    nb_iter = ckpt['nb_iter'] + 1
    # best_eval = ckpt['best_re']
    # best_iter = ckpt['best_iter']
else:
    nb_iter = 1
    best_eval = - math.inf
    best_iter = 0

## init
avg_re, avg_kldZ, avg_kldW, avg_lr = 0, 0, 0, 0

## warm-up
while nb_iter <= args.warmup_iter:
    for batch_data in trainloader:

        warmup_beta = nb_iter / args.warmup_iter
        
        batch_data = batch_data.to(args.device)
        batch_data = batch_data.permute(2, 0, 1) # (bs, x_dim, T) -> (T, bs, x_dim)
        batch = {'x': batch_data}
        batch = net(batch)

        loss_re = optim_goal.loss_isd(batch['x'], torch.exp(batch['y']))
        loss_kldZ = optim_goal.loss_kld(batch['z_mean'], batch['z_logvar'], batch['z_mean_p'], batch['z_logvar_p'])
        loss_kldW = optim_goal.loss_kld(batch['w_mean'], batch['w_logvar'])

        optimizer.zero_grad()
        if args.stocasticity == 'stochastic':
            loss = loss_re + warmup_beta * args.beta_z * loss_kldZ + warmup_beta * args.beta_w * loss_kldW
        elif args.stocasticity == 'deterministic':
            loss = loss_re
        loss.backward()
        optimizer.step()

        current_lr = utils.warmup_learning_rate(optimizer, nb_iter, args.warmup_iter, args.max_lr)
        
        avg_re += loss_re.item()
        avg_kldZ += loss_kldZ.item()
        avg_kldW += loss_kldW.item()
        avg_lr += current_lr

        if nb_iter % args.print_iter == 0:
            avg_re /= args.print_iter
            avg_kldZ /= args.print_iter
            avg_kldW /= args.print_iter
            avg_lr /= args.print_iter

            msg = "Warmup iter {:d} : lr {:.8f} \t Re. {:.2f} \t kldZ {:.3f} \t kldW {:.3f}".format(nb_iter, avg_lr, avg_re, avg_kldZ, avg_kldW)
            logger.info(msg)

            tr_writer.add_scalar('Loss/loss_recon', avg_re, global_step=nb_iter)
            tr_writer.add_scalar('Loss/loss_kldZ', avg_kldZ, global_step=nb_iter)
            tr_writer.add_scalar('Loss/loss_kldW', avg_kldW, global_step=nb_iter)
            tr_writer.add_scalar('Lr', avg_lr, global_step=nb_iter)

            avg_re, avg_kldZ, avg_kldW, avg_lr = 0, 0, 0, 0

        if nb_iter == args.warmup_iter:
            msg, save_best, best_iter, best_eval = evaluation(args, net, valdataset, val_writer, current_iter=nb_iter, best_eval=best_eval, best_iter=best_iter)
            logger.info(msg)
            if save_best:
                logger.info("\t-->Loss improved!!! Save best!!!")
                torch.save({'net_state' : net.state_dict(),
                            'optim_state' : optimizer.state_dict(),
                            'sched_state' :scheduler.state_dict(),
                            'nb_iter' : nb_iter,
                            'best_re': best_eval,
                            'best_iter': best_iter,
                            'args': args
                            }, os.path.join(args.out_dir, 'net_best.pth'))
                net_best = copy.deepcopy(net)
            # save latest model
            torch.save({'net_state' : net.state_dict(),
                'optim_state' : optimizer.state_dict(),
                'sched_state' :scheduler.state_dict(),
                'nb_iter' : nb_iter,
                'best_re': best_eval,
                'best_iter': best_iter,
                'args': args
                }, os.path.join(args.out_dir, 'net_latest.pth'))

        nb_iter += 1

        if nb_iter > args.warmup_iter: 
            break
            
## Training
avg_re, avg_kldZ, avg_kldW, avg_lr = 0, 0, 0, 0
while (nb_iter) <= args.total_iter + args.warmup_iter:
    for batch_data in trainloader:

        batch_data = batch_data.to(args.device)
        batch_data = batch_data.permute(2, 0, 1) # (bs, x_dim, T) -> (T, bs, x_dim)
        batch = {'x': batch_data}
        batch = net(batch)

        loss_re = optim_goal.loss_isd(batch['x'], torch.exp(batch['y']))
        loss_kldZ = optim_goal.loss_kld(batch['z_mean'], batch['z_logvar'], batch['z_mean_p'], batch['z_logvar_p'])
        loss_kldW = optim_goal.loss_kld(batch['w_mean'], batch['w_logvar'])

        optimizer.zero_grad()
        if args.stocasticity == 'stochastic':
            loss = loss_re + args.beta_z * loss_kldZ + args.beta_w * loss_kldW
        elif args.stocasticity == 'deterministic':
            loss = loss_re
        loss.backward()
        optimizer.step()

        scheduler.step()
        current_lr = scheduler.get_last_lr()[0]

        avg_re += loss_re.item()
        avg_kldZ += loss_kldZ.item()
        avg_kldW += loss_kldW.item()
        avg_lr += current_lr

        if nb_iter % args.print_iter == 0:
            avg_re /= args.print_iter
            avg_kldZ /= args.print_iter
            avg_kldW /= args.print_iter
            avg_lr /= args.print_iter

            msg = "Training iter {:d} : lr {:.8f} \t Re. {:.2f} \t kldZ {:.3f} \t kldW {:.3f}".format(nb_iter, avg_lr, avg_re, avg_kldZ, avg_kldW)
            logger.info(msg)

            tr_writer.add_scalar('Loss/loss_recon', avg_re, global_step=nb_iter)
            tr_writer.add_scalar('Loss/loss_kldZ', avg_kldZ, global_step=nb_iter)
            tr_writer.add_scalar('Loss/loss_kldW', avg_kldW, global_step=nb_iter)
            tr_writer.add_scalar('Lr', avg_lr, global_step=nb_iter)

            avg_re, avg_kldZ, avg_kldW, avg_lr = 0, 0, 0, 0

        if nb_iter % args.eval_iter ==  0 :
            msg, save_best, best_iter, best_eval = evaluation(args, net, valdataset, val_writer, current_iter=nb_iter, best_eval=best_eval, best_iter=best_iter)
            logger.info(msg)
            if save_best:
                logger.info("\t-->Loss improved!!! Save best!!!")
                torch.save({'net_state' : net.state_dict(),
                            'optim_state' : optimizer.state_dict(),
                            'sched_state' :scheduler.state_dict(),
                            'nb_iter' : nb_iter,
                            'best_re': best_eval,
                            'best_iter': best_iter,
                            'args': args
                            }, os.path.join(args.out_dir, 'net_best.pth'))
                net_best = copy.deepcopy(net)
                        
            # save latest model
            torch.save({'net_state' : net.state_dict(),
                'optim_state' : optimizer.state_dict(),
                'sched_state' :scheduler.state_dict(),
                'nb_iter' : nb_iter,
                'best_re': best_eval,
                'best_iter': best_iter,
                'args': args
                }, os.path.join(args.out_dir, 'net_latest.pth'))

        nb_iter += 1

        if nb_iter > args.total_iter + args.warmup_iter: 
            break

## Test
logger.info("*"*100)
logger.info("Test with the best model")
logger.info("*"*100)
test(args, net_best, logger, test_val=False)
test(args, net_best, logger, test_val=True)
logger.info("*"*100)
logger.info("Test with the latest model")
logger.info("*"*100)
test(args, net, logger, test_val=False)
test(args, net, logger, test_val=True)

