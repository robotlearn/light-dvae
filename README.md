# Implementation of LigHT-DVAE
This repo contains the code for implementations of LigHT-DVAE.

## Environment
The environment configurations are defined in the file container.def. The required python packages and associated versions are defined in the file requirements.txt.

## Data
In the original paper, the models are trained either on the WSJ0 dataset or on the VoiceBank Corpus, which are the commonly used dataset for speech processing related tasks. You can download the corresponding datasets and use the argument '--dataset-name' to specify the dataset name and the arguments '--train-dir', '--val-dir' to specify the dataset directories.

## Model training
To train the LigHT-DVAE model, you can run the following command:

`python train.py --exp-name YOUR_EXPERIMENT_NAME --model-name 'light_dvae' --train-dir YOUR_TRAIN_DIR --val-dir YOUR_VAL_DIR`

To train the HiT-DVAE model, you can run the following command:

`python train.py --exp-name YOUR_EXPERIMENT_NAME --model-name 'hit_dvae' --train-dir YOUR_TRAIN_DIR --val-dir YOUR_VAL_DIR`

## Contact
For any further question, contact me at xiaoyu[dot]lin[at]inria[dot]fr
