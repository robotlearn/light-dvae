import argparse


def get_args_parser():
    parser = argparse.ArgumentParser(description='Hierarchical Transformer DVAE training for speech',
                                     add_help=True,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    ## dataloader
    parser.add_argument('--dataset-name', type=str, default='voicebank',choices=['wsj', 'voicebank'], help='dataset name')
    parser.add_argument('--train-dir', type=str, default='./data/VoiceBankDemand/clean_trainset_26spk_wav_16k', help='training directory')
    parser.add_argument('--val-dir', type=str, default='./data/VoiceBankDemand/clean_valset_2spk_wav_16k', help='validation directory')
    parser.add_argument('--test-dir', type=str, default='./data/VoiceBankDemand/clean_testset_wav_16k', help='test directory')
    parser.add_argument('--nb-worker', type=int, default=8, help='number of data loading workers')
    parser.add_argument('--seq-len', type=int, default=100, help='speech sequence length')
    parser.add_argument('--batch-size', type=int, default=128, help='batch size')

    ## optimization
    parser.add_argument('--total-iter', type=int, default=200000, help='number of total iterations to run')
    parser.add_argument('--max-lr', type=float, default=5e-5, help='max learning rate')
    parser.add_argument('--min-lr', type=float, default=1e-8, help='min learning rate')
    parser.add_argument('--warmup-iter', type=int, default=5000, help='number of warmup iterations')
    parser.add_argument('--policy', type=str, default='cosine', choices=['linear', 'cosine', 'step', 'multistep', 'plateau'], help='learning rate schedule policy')
    parser.add_argument('--lr-scheduler', type=int, default=[60000], nargs='+', help='learning rate schedule (iterations), only used for MultiStepLR')
    parser.add_argument('--weight-decay', type=float, default=1e-5, help='weight decay')
    parser.add_argument('--beta-z', type=float, default=1e-2, help='hyper-parameter for beta-vae on z')
    parser.add_argument('--beta-w', type=float, default=1e-2, help='hyper-parameter for beta-vae on w')
    parser.add_argument('--decay-option', type=str, default='all', choices=['all', 'light'], help='disable weight decay for some layers')

    ## dvae arch
    ### Options to define different variants of architectur
    parser.add_argument('--residual', type=str, default='residual', choices=['residual', 'no_residual'], help='whether to use the residual in decoder, if true, not using the residual')
    parser.add_argument('--decoder-x-mode', type=str, default='inverse_transformer', choices=['standard_transformer', 'inverse_transformer'], help='the mode of transformer decoder predictions, standard_transfermer use x as input and z as memory while inverse_transformer use z as input and x as memory')
    parser.add_argument('--stocasticity', type=str, default='stochastic', choices=['stochastic', 'deterministic'], help='whether to define the model in stochastic or deterministic way')

    ### general
    parser.add_argument('--x-dim', type=int, default=513, help='dimension of input feature')
    parser.add_argument('--z-dim', type=int, default=16, help='dimension of latent feature z')
    parser.add_argument('--w-dim', type=int, default=32, help='dimension of latent feature w')
    parser.add_argument('--dropout', type=float, default=0, help='dropout ratio')
    parser.add_argument('--activation', type=str, default='relu', choices=['relu', 'gelu', 'silu', 'tanh'], help='activation function for dense layers')
    parser.add_argument('--fe-dim-list', type=int, nargs='+', default=[256], help='list of dimensions for feature extraction')
    ### inference on w
    parser.add_argument('--infW-dim', type=int, default=512, help='dimension of LSTM for the inference on w')
    parser.add_argument('--infW-num-layer', type=int, default=2, help='number of LSTM layers for the inference on w')
    ### transformer for infZ, genZ, genX
    parser.add_argument('--trans-dim', type=int, default=256, help='transformer dimension')
    parser.add_argument('--num-head', type=int, default=1, help='heads in the transformer')
    parser.add_argument('--ff-size', type=int, default=1024, help='ff size')
    parser.add_argument('--num-layer', type=int, default=4, help='layers in the transformer')

    ## STFT params
    parser.add_argument('--wlen-sec', type=float, default=64e-3, help='STFT, window length on ms')
    parser.add_argument('--hop-percent', type=float, default=0.25, help='STFT, hop percentage on window length')
    parser.add_argument('--fs', type=float, default=16000, help='STFT, speech frequence')
    parser.add_argument('--zp-percent', type=float, default=0, help='STFT, zp percentage')

    ## output directory
    parser.add_argument('--out-dir', type=str, default='./output', help='output directory')
    parser.add_argument('--exp-name', type=str, default='hitdvae_speech_debug', help='name of the experiment, will create a file inside out-dir')

    ## other
    parser.add_argument('--model-name', type=str, default='light_dvae', choices=['light_dvae', 'hit_dvae'], help='model name')
    parser.add_argument('--resume-pth', type=str, default=None, help='resume pth')
    parser.add_argument('--print-iter', type=int, default=500, help='print frequency')
    parser.add_argument('--eval-iter', type=int, default=5000, help='evaluation frequency')
    parser.add_argument('--device', type=str, default='cuda',choices=['cuda', 'cpu'], help='device')
    parser.add_argument('--seed', type=int, default=209, help='seed for initializing training')
    parser.add_argument('--num-generate', type=int, default=50, help='number of sequence to generate.')

    return parser.parse_args()