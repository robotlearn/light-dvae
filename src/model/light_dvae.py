"""
Copyright (c) 2023 by Inria
Authoried by Xiaoyu LIN (xiaoyu.lin@inrai.fr)
License agreement in LICENSE.txt

"""

import torch
from torch import nn
import numpy as np
import copy
from collections import OrderedDict
from .base import TransformerDecoderLayer, TransformerDecoderLayer_noresidual, TransformerEncoderLayer

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-np.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        
        self.register_buffer('pe', pe)

    def forward(self, x):
        # not used in the final model
        x = x + self.pe[:x.shape[0], :]
        return self.dropout(x)


class LigHTDVAE(nn.Module):

    def __init__(self, args):

        super().__init__()
        self.args = args
        self.x_dim = self.args.x_dim
        self.z_dim = self.args.z_dim
        self.w_dim = self.args.w_dim
        self.activation = self._get_activation_fn(self.args.activation)
        self.build()

    def _get_activation_fn(self, activation):
        if activation == "relu":
            return nn.ReLU()
        elif activation == "gelu":
            return nn.GELU()
        elif activation == "tanh":
            return nn.Tanh()
        elif activation == 'silu':
            return nn.SiLU()
        raise RuntimeError("activation should be relu/gelu/tanh/silu, not {}".format(activation))

    def build(self):

        ##################
        ### Feature x ####
        ##################
        if len(self.args.fe_dim_list) == 0:
            self.fe_infX = nn.Identity()
            dim_feX = self.x_dim
        else:
            dic_layers = OrderedDict()
            dim_in = self.x_dim
            for n in range(len(self.args.fe_dim_list)):
                dic_layers['linear'+str(n)] = nn.Linear(dim_in, self.args.fe_dim_list[n])
                dic_layers['activation'+str(n)] = self.activation
                dic_layers['dropout'+str(n)] = nn.Dropout(p=self.args.dropout)
                dim_in = self.args.fe_dim_list[n]
            self.fe_infX = nn.Sequential(dic_layers)
            dim_feX = self.args.fe_dim_list[-1]

        self.fe_genX = copy.deepcopy(self.fe_infX)

        ############################
        ### Positional Encoding ####
        ############################
        self.pos_enc = PositionalEncoding(self.args.trans_dim, dropout=0.1)

        ##################
        ### Inference ####
        ##################
        # inf w
        self.rnn_w = nn.LSTM(dim_feX, self.args.infW_dim, self.args.infW_num_layer, bidirectional=True)
        self.inf_w = nn.Linear(2*self.args.infW_dim, 2*self.args.w_dim)
        # inf z_{1:T}
        seqTransEncoderLayer = TransformerEncoderLayer(d_model=self.args.trans_dim,
                                                    nhead=self.args.num_head,
                                                    dim_feedforward=self.args.ff_size,
                                                    dropout=0.1,
                                                    activation='gelu')
        
        self.emb_infXW = nn.Linear(dim_feX+self.w_dim, self.args.trans_dim)
        self.seqTransEnc = nn.TransformerEncoder(seqTransEncoderLayer, num_layers=self.args.num_layer)
        self.inf_z = nn.Linear(self.args.trans_dim, 2*self.z_dim)


        ###################
        ### Generation ####
        ###################
        if self.args.residual == 'no_residual':
            seqTransDecoderLayer = TransformerDecoderLayer_noresidual(d_model=self.args.trans_dim,
                                                    nhead=self.args.num_head,
                                                    dim_feedforward=self.args.ff_size,
                                                    dropout=0.1,
                                                    activation='gelu')
        elif self.args.residual == 'residual':
            seqTransDecoderLayer = TransformerDecoderLayer(d_model=self.args.trans_dim,
                                                        nhead=self.args.num_head,
                                                        dim_feedforward=self.args.ff_size,
                                                        dropout=0.1,
                                                        activation='gelu')
        
        if self.args.decoder_x_mode == 'standard_transformer':
            self.emb_genXW = nn.Linear(dim_feX+self.w_dim, self.args.trans_dim)
            self.emb_genZ = nn.Linear(self.z_dim, self.args.trans_dim)
        elif self.args.decoder_x_mode == 'inverse_transformer':
            self.emb_genZW = nn.Linear(self.z_dim+self.w_dim, self.args.trans_dim)
            self.emb_genX = nn.Linear(dim_feX, self.args.trans_dim)
        self.seqTransDec = nn.TransformerDecoder(seqTransDecoderLayer, num_layers=self.args.num_layer)
        self.gen_x = nn.Linear(self.args.trans_dim, self.x_dim)
        self.gen_z = nn.Linear(self.args.trans_dim, 2*self.z_dim)


        ###################
        #### Optim Enc ####
        ###################
        self.layer_to_optim = [self.fe_infX, self.rnn_w, self.inf_w, self.seqTransEnc, self.inf_z]


    def reparameterization(self, mean, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return eps.mul(std).add_(mean)


    def inference(self, x):
        # Feature x
        seq_len, bs, x_dim = x.shape
        fe_x = self.fe_infX(x)
        
        # Infer content w
        _, (_w, _)= self.rnn_w(fe_x)
        _w = _w.reshape(2, self.args.infW_num_layer, bs, self.args.infW_dim)[:, -1] # last layer
        _w = _w.permute(1, 0, 2).reshape(bs, -1) # (2, bs, dim_rnn) -> (bs, dim_rnn*2)
        _w = self.inf_w(_w)
        w_mean = _w[:,:self.w_dim]
        w_logvar = _w[:,self.w_dim:]
        if self.args.stocasticity == 'stochastic':
            w = self.reparameterization(w_mean, w_logvar)
        elif self.args.stocasticity == 'deterministic':
            w = w_mean

        # Infer dynamic z
        w_expand = w.expand(seq_len, bs, self.w_dim)
        xw_cat = torch.cat((fe_x, w_expand), dim=-1)
        emb_xw = self.emb_infXW(xw_cat)
        emb_xw = self.pos_enc(emb_xw)
        out = self.seqTransEnc(emb_xw)
        _z = self.inf_z(out)
        z_mean = _z[..., :self.z_dim]
        z_logvar = _z[..., self.z_dim:]
        if self.args.stocasticity == 'stochastic':
            z = self.reparameterization(z_mean, z_logvar)
        elif self.args.stocasticity == 'deterministic':
            z = z_mean

        att_map_list_enc = []
        for layer in self.seqTransEnc.layers:
            att_map_list_enc.append(layer.sa_att)

        # return z, z_mean, z_logvar, w, w_mean, w_logvar, att_map_list_enc
        return z, z_mean, z_logvar, w, w_mean, w_logvar


    def generation(self, x, z, w):
        
        # Prepare input
        seq_len, bs, x_dim = x.shape
        z_0 = torch.zeros(1, bs, self.z_dim).to(self.args.device)
        x_0 = torch.zeros(1, bs, self.x_dim).to(self.args.device)
        x_tm1 = torch.cat((x_0, x[:-1]), dim=0)
        z_tm1 = torch.cat((z_0, z[:-1]), dim=0)
        fe_x = self.fe_genX(x_tm1)
        w_expand = w.expand(seq_len, bs, self.w_dim)
        causalMask = torch.triu(torch.ones(seq_len, seq_len) * float('-inf'), diagonal=1).to(self.args.device)

        
        if self.args.decoder_x_mode == 'standard_transformer':
            xw_cat = torch.cat((fe_x, w_expand), dim=-1)
            emb_xw = self.emb_genXW(xw_cat)
            emb_xw = self.pos_enc(emb_xw)
            
            # Generate x
            emb_z_x = self.emb_genZ(z)
            out_y = self.seqTransDec(tgt=emb_xw, memory=emb_z_x, tgt_mask=causalMask, memory_mask=causalMask)
            y = self.gen_x(out_y)

            # Generate z
            emb_ztm1_z = self.emb_genZ(z_tm1)
            out_z = self.seqTransDec(tgt=emb_xw, memory=emb_ztm1_z, tgt_mask=causalMask, memory_mask=causalMask)
            _z = self.gen_z(out_z)
            z_mean_p = _z[..., :self.z_dim]
            z_logvar_p = _z[..., self.z_dim:]                
        
        elif self.args.decoder_x_mode == 'inverse_transformer':
            # Generate x
            zw_cat = torch.cat((z, w_expand), dim=-1)
            emb_zw_x = self.emb_genZW(zw_cat)
            emb_zw_x = self.pos_enc(emb_zw_x)
            emb_x = self.emb_genX(fe_x)
            out_y = self.seqTransDec(tgt=emb_zw_x, memory=emb_x, tgt_mask=causalMask, memory_mask=causalMask)
            y = self.gen_x(out_y)

            # Generate z
            ztm1w_cat = torch.cat((z_tm1, w_expand), dim=-1)
            emb_zw_z = self.emb_genZW(ztm1w_cat)
            emb_zw_z = self.pos_enc(emb_zw_z)
            out_z = self.seqTransDec(tgt=emb_zw_z, memory=emb_x, tgt_mask=causalMask, memory_mask=causalMask)
            _z = self.gen_z(out_z)
            z_mean_p = _z[..., :self.z_dim]
            z_logvar_p = _z[..., self.z_dim:]            
           
        return y, z_mean_p, z_logvar_p


    def forward(self, batch):

        # need input:  batch['x'] (seq_len, bs, x_dim)
        batch['z'], batch['z_mean'], batch['z_logvar'], batch['w'], batch['w_mean'], batch['w_logvar'] = self.inference(batch['x'])
        batch['y'], batch['z_mean_p'], batch['z_logvar_p'] = self.generation(batch['x'], batch['z'], batch['w'])

        return batch


    def generation_x(self, z, w):
        
        seq_len, bs, _ = z.shape
        gen_x = torch.zeros(1, bs, self.x_dim).to(self.args.device)
        w_expand = w.expand(seq_len, bs, self.w_dim)

        if self.args.decoder_x_mode == 'standard_transformer':
            emb_z = self.emb_genZ(z)

            for i in range(seq_len):
                len_inp = gen_x.shape[0]
                fe_x = self.fe_genX(gen_x)
                xw_cat = torch.cat((fe_x, w_expand[:len_inp]), dim=-1)
                emb_xw = self.emb_genXW(xw_cat)
                emb_xw = self.pos_enc(emb_xw)
                causalMask = torch.triu(torch.ones(len_inp, len_inp) * float('-inf'), diagonal=1).to(self.args.device)

                out = self.seqTransDec(tgt=emb_xw, memory=emb_z[:len_inp], tgt_mask=causalMask, memory_mask=causalMask)
                y = self.gen_x(out)
                # For the audio data
                y = torch.exp(y[-1]).unsqueeze(0)
                gen_x = torch.cat((gen_x, y), dim=0)
        elif self.args.decoder_x_mode == 'inverse_transformer':
            zw_cat = torch.cat((z, w_expand), dim=-1)
            emb_zw = self.emb_genZW(zw_cat)
            emb_zw = self.pos_enc(emb_zw)

            for i in range(seq_len):
                len_inp = gen_x.shape[0]
                fe_x = self.fe_genX(gen_x)
                emb_x = self.emb_genX(fe_x)
                causalMask = torch.triu(torch.ones(len_inp, len_inp) * float('-inf'), diagonal=1).to(self.args.device)

                out = self.seqTransDec(tgt=emb_zw[:len_inp], memory=emb_x, tgt_mask=causalMask, memory_mask=causalMask)
                y = self.gen_x(out)
                # For the audio data
                y = torch.exp(y[-1]).unsqueeze(0)
                gen_x = torch.cat((gen_x, y), dim=0)

        return gen_x[1:]

    def generation_from_prior(self):
        seq_len = self.args.seq_len
        bs = self.args.batch_size
        gen_x = torch.zeros(1, bs, self.x_dim).to(self.args.device)
        gen_z = torch.zeros(1, bs, self.z_dim).to(self.args.device)
        gen_w = torch.normal(mean=0, std=1, size=(1, bs, self.w_dim)).to(self.args.device)
        
        if self.args.decoder_x_mode == 'inverse_transformer':
            for i in range(seq_len):
                # Generation of z using [z_1:t-1, w] and s_1:t-1
                len_inp = gen_z.shape[0]
                gen_w_expand = gen_w.expand(len_inp, bs, self.w_dim)
                zw_cat_genz = torch.cat((gen_z, gen_w_expand), dim=-1)
                emb_zw_genz = self.emb_genZW(zw_cat_genz)
                emb_zw_genz = self.pos_enc(emb_zw_genz)

                fe_x = self.fe_genX(gen_x)
                emb_x = self.emb_genX(fe_x)                
                causalMask = torch.triu(torch.ones(len_inp, len_inp) * float('-inf'), diagonal=1).to(self.args.device)
                out_z = self.seqTransDec(tgt=emb_zw_genz[:len_inp], memory=emb_x[:len_inp], tgt_mask=causalMask, memory_mask=causalMask)
                _z = self.gen_z(out_z)
                z_mean_p = _z[..., :self.z_dim]
                z_logvar_p = _z[..., self.z_dim:]

                z = self.reparameterization(z_mean_p[-1], z_logvar_p[-1])

                gen_z = torch.cat((gen_z, z.unsqueeze(0)), dim=0)

                # Generation of s using [z_1:t, w] and s_1:t-1
                gen_z_x = gen_z[1:]
                zw_cat_genx = torch.cat((gen_z_x, gen_w_expand), dim=-1)
                emb_zw_genx = self.emb_genZW(zw_cat_genx)
                emb_zw_genx = self.pos_enc(emb_zw_genx)

                out_y = self.seqTransDec(tgt=emb_zw_genx[:len_inp], memory=emb_x[:len_inp], tgt_mask=causalMask, memory_mask=causalMask)
                y = self.gen_x(out_y)
                y = torch.exp(y[-1]).unsqueeze(0)
                gen_x = torch.cat((gen_x, y), dim=0)

        return gen_x[1:]

        


