import os
import random
import numpy as np
import torch
import librosa
import soundfile as sf
from torch.utils import data

def TrainLoader(args):
    dataset = SpeechSequences(data_dir=args.train_dir, seq_len=args.seq_len, args=args)
    data_num = dataset.__len__()
    data_loader = data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True, num_workers=args.nb_worker, pin_memory=True)

    return data_loader, data_num


def ValDataset(args):
    dataset = SpeechDataset(data_dir=args.val_dir)
    data_num = len(dataset.audio_list)

    return dataset, data_num

class SpeechSequences(data.Dataset):
    def __init__(self, data_dir, seq_len, args, trim=True):
        super().__init__()
        self.data_dir = data_dir
        self.seq_len = seq_len
        self.trim = trim
        self.fs = args.fs
        self.wlen = args.wlen_sec * args.fs
        self.wlen = int(np.power(2, np.ceil(np.log2(self.wlen))))
        self.hop = int(args.hop_percent * self.wlen)
        self.nfft = self.wlen + args.zp_percent * self.wlen
        self.win = torch.sin(torch.arange(0.5, self.wlen+0.5) / self.wlen * np.pi)
        self._load()

    def _load(self):
        self.audio_list = librosa.util.find_files(self.data_dir, ext='wav')

    def __len__(self):
        return len(self.audio_list)

    def __getitem__(self, index):

        wavfile = self.audio_list[index]
        x, fs_x = sf.read(wavfile)
        if self.trim:
            x, idx = librosa.effects.trim(x, top_db=30)

        if len(x) // self.hop < self.seq_len:
            sample = self.__getitem__(np.random.randint(self.__len__()))
        else:
            x = x/np.max(np.abs(x))
            audio_spec = torch.stft(torch.from_numpy(x), n_fft=self.nfft, hop_length=self.hop, 
                                win_length=self.wlen, window=self.win, center=True, 
                                pad_mode='reflect', normalized=False, onesided=True,  return_complex=True)
            sample = (audio_spec.abs() ** 2).float().numpy()
            T = sample.shape[1]
            start = np.random.randint(T-self.seq_len+1)
            end = start + self.seq_len
            sample = sample[:, start:end]

        return sample

class SpeechDataset():
    def __init__(self, data_dir, trim=True):
        super().__init__()
        self.data_dir = data_dir
        self.trim = trim
        self._load()

    def _load(self):
        self.audio_list = librosa.util.find_files(self.data_dir)

    def batch_generator(self, seq_len=100, hop=256, batch_size=128):
        audio_nums = len(self.audio_list)
        required_lens = seq_len*hop-hop
        i = 0
        j = 0
        sample = []
        while i < audio_nums:
            wavfile = self.audio_list[i]
            x, fs_x = sf.read(wavfile)
            if self.trim:
                x, idx = librosa.effects.trim(x, top_db=30)
            x = x / np.max(np.abs(x))
            if len(x) >= required_lens:
                start = 0
                end = start + required_lens
                while end < len(x):
                    sample_j = x[start:end]
                    sample.append(sample_j)
                    j += 1
                    if j == batch_size:
                        sample = np.stack(sample, axis=0)
                        yield sample
                        j = 0
                        sample = []
                    start = end
                    end += required_lens

                # sample_j = x[:required_lens]
                # sample.append(sample_j)
                # j += 1

            i += 1
